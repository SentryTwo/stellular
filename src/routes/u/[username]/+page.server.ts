import type { PageServerLoad } from "./$types";
import { error } from "@sveltejs/kit";

import { comms, auth } from "$lib/server_stores";

export const load: PageServerLoad = async ({ cookies, params }) => {
    let data: { [key: string]: any } = {};

    // get profile
    const profile = await auth.GetUserFromName(params.username);
    if (!profile[0] || !profile[2]) return error(404, profile[1]);
    data.profile = profile[2];

    // get groups
    const groups = await comms.GetUserGroups(params.username);
    if (!groups[0] || !groups[2]) return error(404, "User Invalid");
    data.groups = [];

    for (const group of groups[2]) {
        // fill group information
        const g = await comms.GetGroupByID(group[0]);
        if (!g[0] || !g[2]) continue;
        data.groups.push(g[2]);
    }

    // get auth cookie
    const cookie = cookies.get("__Secure-StarID");

    if (cookie) {
        // get user
        const user = await auth.GetUserFromID(cookie);
        if (!user[0] || !user[2]) return error(401, "Session Invalid");
        data.user = user[2];

        // check for an existing dm
        const dm =
            (await comms.GetChannelByID(`@direct:${user[2].ID}:${profile[2].ID}`)) ||
            (await comms.GetChannelByID(`@direct:${profile[2].ID}:${user[2].ID}`));

        data.existing_dm = dm[2];
    }

    // return
    return data;
};
