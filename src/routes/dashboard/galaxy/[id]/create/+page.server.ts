import type { PageServerLoad } from "./$types";
import { error } from "@sveltejs/kit";

import { MEMBERSHIP_STATUS } from "$lib/db/CommunicationDB";
import { comms, auth } from "$lib/server_stores";

export const load: PageServerLoad = async ({ cookies, params }) => {
    let data: { [key: string]: any } = {};

    // get auth cookie
    const cookie = cookies.get("__Secure-StarID");

    if (cookie) {
        // get user
        const user = await auth.GetUserFromID(cookie);
        if (!user[0] || !user[2]) return error(401, "Session Invalid");
        data.user = user[2];

        // get current galaxy
        const galaxy = await comms.GetGroupByID(params.id);
        if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);
        data.galaxy = galaxy;

        // get user membership in galaxy
        const membership = await comms.GetUserGroupMembership(
            user[2].Username,
            params.id
        );

        // user MUST be a member AND must not be banned (< 1)
        if (!membership[0] || membership[2] < MEMBERSHIP_STATUS.MEMBER)
            return error(401, "Unauthorized");
    } else return error(401, "Unauthorized"); // must be logged in

    if (!data.galaxy) return error(401, "Unauthorized");

    // return
    return data;
};
