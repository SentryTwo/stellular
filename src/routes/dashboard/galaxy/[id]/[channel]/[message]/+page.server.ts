import type { PageServerLoad } from "./$types";
import { error } from "@sveltejs/kit";

import { comms, auth } from "$lib/server_stores";

export const load: PageServerLoad = async ({ cookies, params }) => {
    let data: { [key: string]: any } = {};

    // get auth cookie
    const cookie = cookies.get("__Secure-StarID");

    if (cookie) {
        // get user
        const user = await auth.GetUserFromID(cookie);
        if (!user[0] || !user[2]) return error(401, "Session Invalid");
        data.user = user[2];

        // get current galaxy
        const galaxy = await comms.GetGroupByID(params.id);
        if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);
        data.galaxy = galaxy;

        // get current planet
        const planet = await comms.GetChannelByID(params.channel);
        if (!planet[0] || !planet[2]) return error(404, planet[1]);
        data.planet = planet;

        // if we're looking at a direct message channel, replace planet name with the name of the other user!
        if (galaxy[2].ID === "@me") {
            const otherUserID = data.planet[2].ID.split("@direct:")[1]
                .replace(user[2].ID, "")
                .replace(":", "");

            // get other user
            const otherUser = await auth.GetUserFromID(otherUserID);
            if (!otherUser[0] || !otherUser[2]) return error(404, otherUser[1]);
            data.planet[2].Name = otherUser[2].Username;
        }

        // get message
        const message = await comms.GetMessageByID(params.message);
        if (!message[0] || !message[2]) return error(404, message[1]);
        data.message = message[2];

        // channels can be viewed by anybody by default, however you cannot post without being a member of the galaxy!
        const membership = await comms.GetUserGroupMembership(
            user[2].Username,
            params.id
        );

        data.membership = membership[2];
    }

    if (!data.planet) return error(401, "Unauthorized");

    // return
    return data;
};
