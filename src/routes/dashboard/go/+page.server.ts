import type { PageServerLoad } from "./$types";
import { error } from "@sveltejs/kit";

import { comms, auth } from "$lib/server_stores";

export const load: PageServerLoad = async ({ cookies }) => {
    let data: { [key: string]: any } = {};

    // get auth cookie
    const cookie = cookies.get("__Secure-StarID");

    if (cookie) {
        // get user
        const user = await auth.GetUserFromID(cookie);
        if (!user[0] || !user[2]) return error(401, "Session Invalid");
        data.user = user[2];

        // get groups
        const groups = await comms.GetUserGroups(user[2].Username);
        if (!groups[0] || !groups[2]) return error(404, "User Invalid");
        data.groups = [];

        for (const group of groups[2]) {
            // fill group information
            const g = await comms.GetGroupByID(group[0]);
            if (!g[0] || !g[2]) continue;
            data.groups.push(g[2]);
        }
    } else return error(401, "Unauthorized");

    if (!data.groups) return error(401, "Unauthorized");

    // return
    return data;
};
