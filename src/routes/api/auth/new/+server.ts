import type { RequestHandler } from "./$types";
import { json } from "@sveltejs/kit";

import { VerifyContentType } from "$lib/db/helpers/RequestUtilities";
import { auth } from "$lib/server_stores";

export const POST: RequestHandler = async (props) => {
    // verify content type
    const WrongType = VerifyContentType(props.request, "application/json");
    if (WrongType) return WrongType;

    // get body
    const body = await props.request.json();

    // check expected values
    if (!body.Username)
        return json(
            {
                success: false,
                message: "Missing body.Username",
            },
            { status: 400 }
        );

    // create user and return login token (uuid)
    const user = await auth.CreateUser(body.Username);

    return json(
        {
            success: user[0],
            message: user[1],
            payload: user[2],
        },
        {
            status: user[0] === true ? 200 : 400,
            headers: {
                "Set-Cookie":
                    user[0] === true
                        ? `__Secure-StarID=${
                              user[2]!.ID
                          }; SameSite=Strict; Secure; Path=/; HostOnly=true; HttpOnly=true; Max-Age=${
                              60 * 60 * 24 * 365
                          }`
                        : "",
            },
        }
    );
};
