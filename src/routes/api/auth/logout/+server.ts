import type { RequestHandler } from "./$types";
import { json, error } from "@sveltejs/kit";

import { auth } from "$lib/server_stores";

export const POST: RequestHandler = async (props) => {
    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0]) error(401);

    return json(
        {
            success: user[0],
            message: user[1],
            payload: user[2],
        },
        {
            status: user[0] === true ? 200 : 400,
            headers: {
                // clear cookie (set Max-Age to 0)
                "Set-Cookie":
                    user[0] === true
                        ? `__Secure-StarID=refresh; SameSite=Strict; Secure; Path=/; HostOnly=true; HttpOnly=true; Max-Age=0`
                        : "",
            },
        }
    );
};
