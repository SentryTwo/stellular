import type { RequestHandler } from "./$types";
import { text } from "@sveltejs/kit";

export const GET: RequestHandler = async ({ url }) => {
    return text(
        `<svg
    version="1.1"
    viewBox="0 0 150 150"
    xmlns="http://www.w3.org/2000/svg"
    width="150px"
    height="150px"
>
    <rect width="100%" height="100%" fill="#fcd94a" />

    <text 
        x="75" 
        y="95" 
        font-size="60" 
        font-family="sans-serif"
        text-anchor="middle" 
        fill="black"
    >
        ${(url.searchParams.get("text") || "stellular").slice(0, 2).toUpperCase()}
    </text>
</svg>
`,
        {
            headers: {
                "Content-Type": "image/svg+xml",
            },
        }
    );
};
