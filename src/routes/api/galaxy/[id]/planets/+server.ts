import type { RequestHandler } from "./$types";
import { json, error } from "@sveltejs/kit";

import { VerifyContentType } from "$lib/db/helpers/RequestUtilities";
import { MEMBERSHIP_STATUS, type Channel } from "$lib/db/CommunicationDB";
import { comms, auth } from "$lib/server_stores";

export const POST: RequestHandler = async (props) => {
    // verify content type
    const WrongType = VerifyContentType(props.request, "application/json");
    if (WrongType) return WrongType;

    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0] || !user[2]) error(401);

    // check user membership in galaxy
    const membership = await comms.GetUserGroupMembership(
        user[2].Username,
        props.params.id
    );

    if (!membership[0] || membership[2] < MEMBERSHIP_STATUS.PROTECTED)
        return error(401, "Unauthorized"); // user MUST be AT LEAST protected (so dms work!)
    // users MUST be owner in order to delete planets! (or galaxies)

    // get body
    const body = await props.request.json();

    // check expected values
    if (!body.Name)
        return json(
            {
                success: false,
                message: "Missing body.Name",
            },
            { status: 400 }
        );

    // get current galaxy
    const galaxy = await comms.GetGroupByID(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // create channel and return
    const res =
        body.User === undefined && galaxy[2].ID !== "@me"
            ? await comms.CreateChannel({
                  Name: body.Name,
                  Galaxy: galaxy[2].ID,
                  Type: body.Type || "feed",
                  // these values will be filled automatically
                  ID: "",
                  $metadata: {} as Channel["$metadata"],
              })
            : await comms.CreateDirectChannel(user[2].ID, body.User);

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};

export const GET: RequestHandler = async (props) => {
    // get current galaxy
    const galaxy = await comms.GetGroupByID(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // get channels and return
    const res = await comms.GetGroupChannels(props.params.id);

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};
