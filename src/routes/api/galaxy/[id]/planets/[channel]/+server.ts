import type { RequestHandler } from "./$types";
import { json, error } from "@sveltejs/kit";

import { MINIMUM_ACTION_REQUIREMENTS } from "$lib/db/CommunicationDB";
import { comms, auth } from "$lib/server_stores";

export const GET: RequestHandler = async (props) => {
    // get current galaxy
    const galaxy = await comms.GetGroupByID(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // get channel and return
    const res = await comms.GetChannelByID(props.params.channel);

    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (user[0] && user[2] && res[0] && res[2])
        if (galaxy[2].ID === "@me") {
            const otherUserID = res[2].ID.split("@direct:")[1]
                .replace(user[2].ID, "")
                .replace(":", "");

            // get other user
            const otherUser = await auth.GetUserFromID(otherUserID);
            if (!otherUser[0] || !otherUser[2]) return error(404, otherUser[1]);
            res[2].Name = otherUser[2].Username;
        }

    // return
    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};

export const DELETE: RequestHandler = async (props) => {
    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0] || !user[2]) error(401);

    // check user membership in galaxy
    const membership = await comms.GetUserGroupMembership(
        user[2].Username,
        props.params.id
    );

    if (!membership[0] || membership[2] < MINIMUM_ACTION_REQUIREMENTS.MANAGE_PLANETS)
        return error(401, "Unauthorized"); // user MUST be AT LEAST an owner

    // get current galaxy
    const galaxy = await comms.GetGroupByID(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // delete channel and return
    const res = await comms.DeleteChannel(props.params.channel);

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};
