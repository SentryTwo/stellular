import type { RequestHandler } from "./$types";
import { json, error } from "@sveltejs/kit";

import { MEMBERSHIP_STATUS, type Group } from "$lib/db/CommunicationDB";
import { VerifyContentType } from "$lib/db/helpers/RequestUtilities";
import { comms, auth } from "$lib/server_stores";

export const POST: RequestHandler = async (props) => {
    // verify content type
    const WrongType = VerifyContentType(props.request, "application/json");
    if (WrongType) return WrongType;

    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0] || !user[2]) error(401);

    // get body
    const body = await props.request.json();

    // check expected values
    if (!body.Name)
        return json(
            {
                success: false,
                message: "Missing body.Name",
            },
            { status: 400 }
        );

    // create group and return
    const res = await comms.CreateGroup({
        Name: body.Name,
        Owner: cookie,
        // these values will be filled automatically
        Invite: "",
        ID: "",
        $metadata: {} as Group["$metadata"],
    });

    // set owner membership
    if (res[0] && res[2] && user[2])
        await comms.SetUserGroupMembership(
            user[2].Username,
            res[2].ID,
            MEMBERSHIP_STATUS.OWNER
        );

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};
