import type { RequestHandler } from "./$types";
import { json, error } from "@sveltejs/kit";

import { VerifyContentType } from "$lib/db/helpers/RequestUtilities";
import { MEMBERSHIP_STATUS } from "$lib/db/CommunicationDB";
import { comms, auth } from "$lib/server_stores";

export const POST: RequestHandler = async (props) => {
    // verify content type
    const WrongType = VerifyContentType(props.request, "application/json");
    if (WrongType) return WrongType;

    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0] || !user[2]) error(401);

    // get galaxy by invite id
    const galaxy = await comms.GetGroupByInvite(props.params.id);
    if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);

    // check user membership in galaxy
    const membership = await comms.GetUserGroupMembership(
        user[2].Username,
        galaxy[2].ID
    );

    if (membership[2] > MEMBERSHIP_STATUS.NOT_MEMBER)
        return error(401, `You're already a member! Role: ${membership[2]}`); // user MUST NOT already be in group

    // set membership
    const res = await comms.SetUserGroupMembership(
        user[2].Username,
        galaxy[2].ID,
        MEMBERSHIP_STATUS.MEMBER
    );

    return json(
        {
            success: res[0],
            message: res[1],
            payload: galaxy,
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};
