/**
 * @file Create Communication DB
 * @name CommunicationDB.ts
 * @license MIT
 */

import { ComputeRandomObjectHash } from "./helpers/Hash";
import SQL, { Config } from "./helpers/SQL";
import AuthDB, { type AuthState } from "./AuthDB";

import {
    type GetObjectCommandOutput,
    type PutObjectCommandOutput,
    type DeleteObjectCommandOutput,
    PutObjectCommand,
    GetObjectCommand,
    DeleteObjectCommand,
    DeleteObjectsCommand,
    ListObjectsV2Command,
} from "@aws-sdk/client-s3";
import { S3Client, auth } from "$lib/server_stores";

import { Database } from "bun:sqlite";
import type { Pool } from "pg";

import { MEMBERSHIP_STATUS } from "./objects/Permissions";
export * from "./objects/Permissions";

import translations from "./objects/translations.json";

// types
export type Group = {
    // a galaxy is essentially a grouping of "planets", which are essentially just text/voice channels
    // each galaxy contains "stars", or users!
    Name: string;
    Owner: string; // ID of user it is owned by (uuid)
    Invite: string; // invite code (ComputeRandomObjectHash)
    ID: string; // (ComputeRandomObjectHash)
    $metadata: {};
};

export type Channel = {
    // ...planet (defined above)
    Name: string;
    Type: "text" | "voice" | "feed"; // only "feed" is supported currently!
    Galaxy: string; // ID of galaxy (Group) it is located in of (ComputeRandomObjectHash)
    ID: string; // (ComputeRandomObjectHash)
    $metadata: {};
};

export type Message = {
    ID: string; // (ComputeRandomObjectHash)
    $metadata: {
        Attachments: string[]; // array containing the ID of all attachments linked to this message
        //                        attachments are stored in "{planet id}/attachments/{attachment id}"
        AuthorData?: AuthState; // filled by server when requesting a message
    };
    // content
    Content: string;
    Author: string; // Username of user it is owned by
    // dates
    Created: number;
    Edited: number;
    // location
    Channel: string; // ID of planet (Channel) it is located in (ComputeRandomObjectHash)
    Reply: string; // ID of other message this message is replying to (ComputeRandomObjectHash)
};

// ...

/**
 * @export
 * @class CommunicationDB
 */
export default class CommunicationDB {
    public static isNew: boolean = true;
    public readonly db: Pool | Database;

    // limits
    public static readonly MinMessageLength = 1;
    public static readonly MaxMessageLength = 100_000;

    public static readonly MinNameLength = 2;
    public static readonly MaxNameLength = 32;

    public static readonly MaxChannelsInGroup = 100;

    private static readonly NameRegex = /^[\w\s\_\-\.\!]+$/gm;

    /**
     * Creates an instance of CommunicationDB.
     * @memberof CommunicationDB
     */
    constructor() {
        // create db link
        const [db, isNew] = Config.use_pg
            ? [
                  SQL.CreatePostgres(
                      Config.pg_host,
                      Config.pg_user,
                      Config.pg_password,
                      Config.pg_database
                  ),
                  false,
              ]
            : SQL.CreateDB("communication", Config.data_directory);

        CommunicationDB.isNew = isNew;
        this.db = db;

        (async () => {
            await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
                // @ts-ignore
                db,
                query: `CREATE TABLE IF NOT EXISTS "Groups" (
                    "Name" varchar(${CommunicationDB.MaxNameLength}),
                    "Owner" varchar(${AuthDB.MaxUsernameLength}),
                    "Invite" varchar(256),
                    "ID" varchar(256),
                    "$metadata" varchar(400000)
                )`,
            });

            await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
                // @ts-ignore
                db,
                query: `CREATE TABLE IF NOT EXISTS "Channels" (
                    "Name" varchar(${CommunicationDB.MaxNameLength}),
                    "Type" varchar(256),
                    "Galaxy" varchar(256),
                    "ID" varchar(256),
                    "$metadata" varchar(400000)
                )`,
            });

            await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
                // @ts-ignore
                db,
                query: `CREATE TABLE IF NOT EXISTS "Messages" (
                    "ID" varchar(256),
                    "Content" varchar(${CommunicationDB.MaxMessageLength}),
                    "Author" varchar(${CommunicationDB.MaxNameLength}),
                    "Created" float,
                    "Edited" float,
                    "Channel" varchar(256),
                    "Reply" varchar(256),
                    "$metadata" varchar(400000)
                )`,
            });

            await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
                // @ts-ignore
                db,
                query: `CREATE TABLE IF NOT EXISTS "Membership" (
                    "Username" varchar(${CommunicationDB.MaxNameLength}),
                    "Galaxy" varchar(256),
                    "Status" float
                )`,
            });
        })();
    }

    // MEMBERSHIP

    // get

    /**
     * @method GetUserGroupMembership
     *
     * @param {string} member Username of user
     * @param {string} galaxy ID of galaxy
     * @return {Promise<[boolean, string, MEMBERSHIP_STATUS]>}
     * @memberof CommunicationDB
     */
    public async GetUserGroupMembership(
        member: string,
        galaxy: string
    ): Promise<[boolean, string, MEMBERSHIP_STATUS]> {
        // if galaxy id is "@me", everybody is a member
        // @me is the galaxy for direct messages, making everybody member ensures nobody can create planets
        // (each planet is a different direct message between 2 members)
        if (galaxy === "@me") return [true, "", MEMBERSHIP_STATUS.PROTECTED];

        // attempt to get asset
        const record = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'SELECT * FROM "Membership" WHERE "Username" = ? AND "Galaxy" = ?',
            params: [member, galaxy],
            get: true,
            use: "Query",
        })) as { Status: MEMBERSHIP_STATUS };

        // return
        if (record) return [true, "", record.Status];
        else
            return [
                false,
                translations.English.error_not_found,
                MEMBERSHIP_STATUS.NOT_MEMBER,
            ];
    }

    // set

    /**
     * @method SetUserGroupMembership
     *
     * @param {string} member Username of user
     * @param {string} galaxy ID of galaxy
     * @param {MEMBERSHIP_STATUS} status
     * @return {Promise<[boolean, string]>}
     * @memberof CommunicationDB
     */
    public async SetUserGroupMembership(
        member: string,
        galaxy: string,
        status: MEMBERSHIP_STATUS
    ): Promise<[boolean, string, MEMBERSHIP_STATUS]> {
        // get current membership status
        const current_status = await this.GetUserGroupMembership(member, galaxy);

        if (current_status[2] === MEMBERSHIP_STATUS.NOT_MEMBER)
            // create membership
            await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
                // @ts-ignore
                db: this.db,
                query: 'INSERT INTO "Membership" VALUES (?, ?, ?)',
                params: [member, galaxy, status],
                transaction: true,
                use: "Prepare",
            });
        // update membership
        else
            await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
                // @ts-ignore
                db: this.db,
                query: 'UPDATE "Membership" SET ("Status") = (?) WHERE "Username" = ? AND "Galaxy" = ?',
                params: [status, member, galaxy],
                transaction: true,
                use: "Prepare",
            });

        // return
        return [true, translations.English.membership_updated, status];
    }

    // GROUPS

    // get

    /**
     * @method GetGroupByID
     *
     * @param {string} id
     * @return {Promise<[boolean, string, Group?]>}
     * @memberof CommunicationDB
     */
    public async GetGroupByID(id: string): Promise<[boolean, string, Group?]> {
        // attempt to get asset
        const record = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'SELECT * FROM "Groups" WHERE "ID" = ?',
            params: [id.toLowerCase()],
            get: true,
            use: "Query",
        })) as Group;

        if (record && typeof record.$metadata === "string")
            record.$metadata = JSON.parse(record.$metadata);

        // return
        if (record) return [true, translations.English.group_exists, record];
        else return [false, translations.English.error_not_found];
    }

    /**
     * @method GetGroupByName
     *
     * @param {string} name
     * @return {Promise<[boolean, string, Group?]>}
     * @memberof CommunicationDB
     */
    public async GetGroupByName(name: string): Promise<[boolean, string, Group?]> {
        // attempt to get asset
        const record = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'SELECT * FROM "Groups" WHERE "Name" = ?',
            params: [name.toLowerCase()],
            get: true,
            use: "Query",
        })) as Group;

        if (record && typeof record.$metadata === "string")
            record.$metadata = JSON.parse(record.$metadata);

        // return
        if (record) return [true, translations.English.group_exists, record];
        else return [false, translations.English.error_not_found];
    }

    /**
     * @method GetGroupByInvite
     *
     * @param {string} invite
     * @return {Promise<[boolean, string, Group?]>}
     * @memberof CommunicationDB
     */
    public async GetGroupByInvite(
        invite: string
    ): Promise<[boolean, string, Group?]> {
        if (invite === "_no_invite") [false, translations.English.error_not_found];

        // attempt to get asset
        const record = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'SELECT * FROM "Groups" WHERE "Invite" = ?',
            params: [invite.toLowerCase()],
            get: true,
            use: "Query",
        })) as Group;

        if (record && typeof record.$metadata === "string")
            record.$metadata = JSON.parse(record.$metadata);

        // return
        if (record) return [true, translations.English.group_exists, record];
        else return [false, translations.English.error_not_found];
    }

    /**
     * @method GetUserGroups
     *
     * @param {string} member
     * @return {Promise<[boolean, string, Array<[string, MEMBERSHIP_STATUS]>]>}
     * @memberof CommunicationDB
     */
    public async GetUserGroups(
        member: string
    ): Promise<[boolean, string, Array<[string, MEMBERSHIP_STATUS]>]> {
        const results = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)(
            {
                // @ts-ignore
                db: this.db,
                query: 'SELECT * FROM "Membership" WHERE "Username" = ?',
                params: [member],
                all: true,
                use: "Query",
            }
        )) as Array<{ Galaxy: string; Status: MEMBERSHIP_STATUS }>;

        // build results
        const _results: Array<[string, MEMBERSHIP_STATUS]> = [];
        for (const group of results) _results.push([group.Galaxy, group.Status]);

        // return
        return [true, translations.English.group_exists, _results];
    }

    // set

    /**
     * @method CreateGroup
     *
     * @param {Group} props
     * @return {Promise<[boolean, string, Group]>}
     * @memberof CommunicationDB
     */
    public async CreateGroup(props: Group): Promise<[boolean, string, Group]> {
        // make sure group doesn't already exist
        const group = await this.GetGroupByName(props.Name);
        if (group[0]) return [false, translations.English.group_exists, props];

        // check name
        if (
            props.Name.length < CommunicationDB.MinNameLength ||
            props.Name.length > CommunicationDB.MaxNameLength
        )
            return [false, translations.English.error_name_length, props];

        if (!props.Name.match(CommunicationDB.NameRegex))
            return [
                false,
                `Name does not pass test: ${CommunicationDB.NameRegex}`,
                props,
            ];

        // create group
        if (!props.ID) {
            // pregenerate ID and Invite if ID is not set
            props.ID = ComputeRandomObjectHash();
            props.Invite = ComputeRandomObjectHash();
        }

        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'INSERT INTO "Groups" VALUES (?, ?, ?, ?, ?)',
            params: [props.Name, props.Owner, props.Invite, props.ID, "{}"],
            transaction: true,
            use: "Prepare",
        });

        // return
        return [true, translations.English.group_created, props];
    }

    // delete

    /**
     * @method DeleteGroup
     *
     * @param {string} id
     * @return {Promise<[boolean, string, Group?]>}
     * @memberof CommunicationDB
     */
    public async DeleteGroup(id: string): Promise<[boolean, string, Group?]> {
        // make sure group already exists
        const group = await this.GetGroupByID(id);
        if (!group[0] || !group[2]) return [false, group[1]];

        // delete group
        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'DELETE FROM "Groups" WHERE "ID" = ?',
            params: [id],
            transaction: true,
            use: "Prepare",
        });

        // delete group channels
        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'DELETE FROM "Channels" WHERE "Galaxy" = ?',
            params: [id],
            transaction: true,
            use: "Prepare",
        });

        // return
        return [true, translations.English.group_deleted, group[2]];
    }

    // PLANETS

    // get

    /**
     * @method GetChannelByID
     *
     * @param {string} id
     * @return {Promise<[boolean, string, Channel?]>}
     * @memberof CommunicationDB
     */
    public async GetChannelByID(id: string): Promise<[boolean, string, Channel?]> {
        // attempt to get asset
        const record = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'SELECT * FROM "Channels" WHERE "ID" = ?',
            params: [id.toLowerCase()],
            get: true,
            use: "Query",
        })) as Channel;

        if (record && typeof record.$metadata === "string")
            record.$metadata = JSON.parse(record.$metadata);

        // return
        if (record) return [true, translations.English.channel_exists, record];
        else return [false, translations.English.error_not_found];
    }

    /**
     * @method GetChannelByName
     *
     * @param {string} name
     * @param {string} group
     * @return {Promise<[boolean, string, Channel?]>}
     * @memberof CommunicationDB
     */
    public async GetChannelByName(
        name: string,
        group: string
    ): Promise<[boolean, string, Channel?]> {
        // attempt to get asset
        const record = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'SELECT * FROM "Channels" WHERE "Name" = ? AND "Galaxy" = ?',
            params: [name.toLowerCase(), group.toLowerCase()],
            get: true,
            use: "Query",
        })) as Channel;

        if (record && typeof record.$metadata === "string")
            record.$metadata = JSON.parse(record.$metadata);

        // return
        if (record) return [true, translations.English.channel_exists, record];
        else return [false, translations.English.error_not_found];
    }

    /**
     * @method GetGroupChannels
     *
     * @param {string} group
     * @param {string | undefined} me Current user ID (for direct messages)
     * @return {Promise<[boolean, string, Array<Channel>]>}
     * @memberof CommunicationDB
     */
    public async GetGroupChannels(
        group: string,
        me?: string
    ): Promise<[boolean, string, Array<Channel>]> {
        // direct messages (@me) galaxy
        if (me && group === "@me") {
            const results = (await (Config.use_pg
                ? SQL.PostgresQueryOBJ
                : SQL.QueryOBJ)({
                // @ts-ignore
                db: this.db,
                query: 'SELECT * FROM "Channels" WHERE "Galaxy" = ? AND "ID" LIKE ?',
                params: [group, `%${me}%`],
                all: true,
                use: "Query",
            })) as Channel[];

            // return
            return [true, translations.English.group_exists, results];
        }

        // ...all normal galaxies
        const results = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)(
            {
                // @ts-ignore
                db: this.db,
                query: 'SELECT * FROM "Channels" WHERE "Galaxy" = ?',
                params: [group],
                all: true,
                use: "Query",
            }
        )) as Channel[];

        // return
        return [true, translations.English.group_exists, results];
    }

    // set

    /**
     * @method CreateChannel
     *
     * @param {Channel} props
     * @return {Promise<[boolean, string, Channel]>}
     * @memberof CommunicationDB
     */
    public async CreateChannel(props: Channel): Promise<[boolean, string, Channel]> {
        // make sure channel doesn't already exist
        const channel = await this.GetChannelByName(props.Name, props.Galaxy);
        if (channel[0]) return [false, translations.English.channel_exists, props];

        // check name
        if (
            props.Name.length < CommunicationDB.MinNameLength ||
            props.Name.length > CommunicationDB.MaxNameLength
        )
            return [false, translations.English.error_name_length, props];

        if (!props.Name.match(CommunicationDB.NameRegex))
            return [
                false,
                `Name does not pass test: ${CommunicationDB.NameRegex}`,
                props,
            ];

        // create group
        if (!props.ID) props.ID = ComputeRandomObjectHash();
        props.Type = props.Type || "feed";

        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'INSERT INTO "Channels" VALUES (?, ?, ?, ?, ?)',
            params: [props.Name, props.Type, props.Galaxy, props.ID, "{}"],
            transaction: true,
            use: "Prepare",
        });

        // return
        return [true, translations.English.channel_created, props];
    }

    /**
     * @method CreateDirectChannel
     *
     * @param {string} user1
     * @param {string} user2
     * @return {Promise<[boolean, string, Channel?]>}
     * @memberof CommunicationDB
     */
    public async CreateDirectChannel(
        user1: string,
        user2: string
    ): Promise<[boolean, string, Channel?]> {
        const ChannelID = `@direct:${user1}:${user2}`;

        // check for an existing dm
        const dm =
            (await this.GetChannelByID(`@direct:${user1}:${user2}`)) ||
            (await this.GetChannelByID(`@direct:${user2}:${user1}`));

        if (dm[2]) return [false, translations.English.channel_exists];

        // create channel and return
        return await this.CreateChannel({
            Name: crypto.randomUUID().substring(0, 32),
            ID: ChannelID,
            Type: "text",
            Galaxy: "@me",
            $metadata: {},
        });
    }

    // delete

    /**
     * @method DeleteChannel
     *
     * @param {string} id
     * @return {Promise<[boolean, string, Channel?]>}
     * @memberof CommunicationDB
     */
    public async DeleteChannel(id: string): Promise<[boolean, string, Channel?]> {
        // make sure channel already exists
        const channel = await this.GetChannelByID(id);
        if (!channel[0] || !channel[2]) return [false, channel[1]];

        // delete channel
        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'DELETE FROM "Channels" WHERE "ID" = ?',
            params: [id],
            transaction: true,
            use: "Prepare",
        });

        // delete channel messages
        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'DELETE FROM "Messages" WHERE "Channel" = ?',
            params: [id],
            transaction: true,
            use: "Prepare",
        });

        // delete channel attachments
        const attachments = await S3Client.send(
            new ListObjectsV2Command({
                Bucket: "stellular",
                Prefix: `${id}/`,
            })
        );

        if (attachments.Contents) {
            const attachment_list: string[] = [];

            for (const attachment of attachments.Contents)
                if (!attachment.Key) continue;
                else attachment_list.push(attachment.Key);

            await this.DeleteAttachments(id, attachment_list, true);
        }

        // return
        return [true, translations.English.channel_deleted, channel[2]];
    }

    // MESSAGES

    // get

    /**
     * @method GetMessageByID
     *
     * @param {string} id
     * @return {Promise<[boolean, string, Message?]>}
     * @memberof CommunicationDB
     */
    public async GetMessageByID(id: string): Promise<[boolean, string, Message?]> {
        // attempt to get asset
        const record = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'SELECT * FROM "Messages" WHERE "ID" = ?',
            params: [id.toLowerCase()],
            get: true,
            use: "Query",
        })) as Message;

        if (record && typeof record.$metadata === "string")
            record.$metadata = JSON.parse(record.$metadata);

        // return
        if (record) return [true, translations.English.message_exists, record];
        else return [false, translations.English.error_not_found];
    }

    /**
     * @method GetChannelMessages
     *
     * @param {string} channel ID of channel
     * @param {number} [limit=100]
     * @param {number} [offset=0]
     * @return {Promise<[boolean, string, Message[]?]>}
     * @memberof CommunicationDB
     */
    public async GetChannelMessages(
        channel: string,
        limit: number = 100,
        offset: number = 0,
        order: "ASC" | "DESC" = "DESC"
    ): Promise<[boolean, string, Message[]?]> {
        // attempt to get asset
        const records = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)(
            {
                // @ts-ignore
                db: this.db,
                query: `SELECT * FROM "Messages" WHERE "Channel" = ? ORDER BY "Created" ${order} LIMIT ? OFFSET ?`,
                params: [channel.toLowerCase(), limit, offset],
                all: true,
                use: "Query",
            }
        )) as Message[];

        if (records)
            for (const message of records) {
                if (typeof message.$metadata === "string")
                    message.$metadata = JSON.parse(message.$metadata);

                const author = await auth.GetUserFromName(message.Author);
                if (!author[0] || !author[2]) continue;
                message.$metadata.AuthorData = author[2];
            }

        // return
        if (records) return [true, translations.English.message_exists, records];
        else return [false, translations.English.error_not_found];
    }

    // set

    /**
     * @method CreateMessage
     *
     * @param {Message} props
     * @return {Promise<[boolean, string, Message]>}
     * @memberof CommunicationDB
     */
    public async CreateMessage(props: Message): Promise<[boolean, string, Message]> {
        // check length
        if (props.Content.length > 100_000 || props.Content.length < 1)
            return [false, translations.English.error_message_length, props];

        // create message
        props.ID = ComputeRandomObjectHash();
        props.Created = new Date().getTime();
        props.Edited = new Date().getTime();

        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'INSERT INTO "Messages" VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
            params: [
                props.ID,
                props.Content,
                props.Author,
                props.Created,
                props.Edited,
                props.Channel,
                props.Reply,
                JSON.stringify(props.$metadata || {}),
            ],
            transaction: true,
            use: "Prepare",
        });

        // return
        return [true, translations.English.message_created, props];
    }

    /**
     * @method EditMessage
     *
     * @param {string} id
     * @param {string} content
     * @return {Promise<[boolean, string, Message?]>}
     * @memberof CommunicationDB
     */
    public async EditMessage(
        id: string,
        content: string
    ): Promise<[boolean, string, Message?]> {
        // make sure message exists
        const message = await this.GetMessageByID(id);
        if (!message[0] || !message[2]) return message;

        // check length
        if (content.length > 100_000 || content.length < 1)
            return [false, translations.English.error_message_length, message[2]];

        // edit message
        const new_edit = new Date().getTime();
        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'UPDATE "Messages" SET ("Content", "Edited") = (?, ?) WHERE "ID" = ?',
            params: [content, new_edit, id],
            transaction: true,
            use: "Prepare",
        });

        // return
        message[2].Content = content;
        message[2].Edited = new_edit;
        return [true, translations.English.message_updated, message[2]];
    }

    // delete

    /**
     * @method DeleteMessage
     *
     * @param {string} id
     * @return {Promise<[boolean, string, Message?]>}
     * @memberof CommunicationDB
     */
    public async DeleteMessage(id: string): Promise<[boolean, string, Message?]> {
        // make sure message already exists
        const message = await this.GetMessageByID(id);
        if (!message[0] || !message[2]) return [false, message[1]];

        // delete message
        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'DELETE FROM "Messages" WHERE "ID" = ?',
            params: [id],
            transaction: true,
            use: "Prepare",
        });

        // delete attachments
        if (
            message[2].$metadata.Attachments &&
            message[2].$metadata.Attachments.length > 0
        )
            await this.DeleteAttachments(
                message[2].Channel,
                message[2].$metadata.Attachments
            );

        // return
        return [true, translations.English.message_deleted, message[2]];
    }

    // MEDIA

    // get

    /**
     * @method GetAttachment
     *
     * @param {string} planet_id
     * @param {string} attachment_id
     * @return {Promise<GetObjectCommandOutput>}
     * @memberof CommunicationDB
     */
    public async GetAttachment(
        planet_id: string,
        attachment_id: string
    ): Promise<GetObjectCommandOutput> {
        const name = `${planet_id}/attachments/${attachment_id}`;

        const res = await S3Client.send(
            new GetObjectCommand({
                Bucket: "stellular",

                // file information
                Key: name,
            })
        );

        return res;
    }

    // set

    /**
     * @methhod UploadAttachment
     *
     * @param {string} planet_id
     * @param {File} attachment
     * @param {string} attachment_id
     * @return {Promise<[string, PutObjectCommandOutput] | [boolean, string]>} [key, PutObjectCommandOutput] | [success, message]
     * @memberof CommunicationDB
     */
    public async UploadAttachment(
        planet_id: string,
        attachment: File,
        attachment_id: string
    ): Promise<[string, PutObjectCommandOutput] | [boolean, string]> {
        if (attachment.size > 15000000)
            return [false, translations.English.error_file_too_large];

        if (!attachment.type.startsWith("image/"))
            return [false, translations.English.error_file_type];

        const name = `${planet_id}/attachments/${attachment_id}`;

        const res = await S3Client.send(
            new PutObjectCommand({
                Bucket: "stellular",
                ServerSideEncryption: "AES256",

                // file information
                Key: name,
                // @ts-ignore don't let the error lie to you, it wants an ArrayBuffer
                Body: await attachment.arrayBuffer(),
                ContentType: attachment.type,
            })
        );

        return [name, res];
    }

    // delete

    /**
     * @methhod DeleteAttachment
     *
     * @param {string} planet_id
     * @param {string} attachment_id
     * @return {Promise<DeleteObjectCommandOutput>}
     * @memberof CommunicationDB
     */
    public async DeleteAttachment(
        planet_id: string,
        attachment_id: string
    ): Promise<DeleteObjectCommandOutput> {
        const name = `${planet_id}/attachments/${attachment_id}`;

        const res = await S3Client.send(
            new DeleteObjectCommand({
                Bucket: "stellular",

                // file information
                Key: name,
            })
        );

        return res;
    }

    /**
     * @methhod DeleteAttachments
     *
     * @param {string} planet_id
     * @param {string[]} attachment_ids
     * @param {boolena} [skipDirectoryAdd=false]
     * @return {Promise<DeleteObjectCommandOutput>}
     * @memberof CommunicationDB
     */
    public async DeleteAttachments(
        planet_id: string,
        attachment_ids: string[],
        skipDirectoryAdd: boolean = false
    ): Promise<DeleteObjectCommandOutput> {
        const names = [];

        for (const attachment_id of attachment_ids)
            if (!skipDirectoryAdd)
                names.push({ Key: `${planet_id}/attachments/${attachment_id}` });
            else names.push({ Key: attachment_id });

        const res = await S3Client.send(
            new DeleteObjectsCommand({
                Bucket: "stellular",

                // file information
                Delete: {
                    Objects: names,
                },
            })
        );

        return res;
    }
}
