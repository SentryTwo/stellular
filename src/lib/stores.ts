/**
 * @file Handle stores
 * @name stores.ts
 * @license MIT
 */

import type { Group, Channel, Message } from "./db/CommunicationDB";
import type { AuthState } from "./db/AuthDB";
import { writable } from "svelte/store";

// ...
export const ThemeStore = writable<"dark" | "light">("light");
export const ContextMenuStore = writable<HTMLElement>();

export const AuthStore = writable<AuthState | undefined>();
export const GalaxyStore = writable<Group | undefined>();
export const PlanetStore = writable<Channel | undefined>();
export const PlanetsStore = writable<Channel[] | undefined>();
export const MessagesStore = writable<Message[] | undefined>();

export const MembershipStore = writable<number | undefined>();

// caches
export type MessagesCache = { [key: string]: Message[] };
export const MessagesCache = writable<MessagesCache>({});

// default export
export default {
    ThemeStore,
    ContextMenuStore,
    AuthStore,
    GalaxyStore,
    PlanetStore,
    PlanetsStore,
    MessagesStore,
    MembershipStore,

    // caches
    MessagesCache,
};
